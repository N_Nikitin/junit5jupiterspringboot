package com.epam.junit5.jupiter.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.util.function.Tuple2;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@RestController
public class EndlessDataController {

    @GetMapping(value = "/endless", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Integer> getEndlessDates() {
        AtomicInteger i = new AtomicInteger();
        Flux<Long> interval = Flux.interval(Duration.ofMillis(300));
        Flux<Integer> values = Flux.fromStream(Stream.generate(i::incrementAndGet));
        return Flux.zip(interval, values).map(Tuple2::getT2);
    }

}

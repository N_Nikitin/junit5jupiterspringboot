package com.epam.junit5.jupiter.controller;

import com.epam.junit5.jupiter.model.MessageWrapper;
import com.google.gson.Gson;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class HelloWorldController {

    private static final String HELLO_WORLD_MESSAGE = "Hello, World!";

    @GetMapping("/hello")
    public Mono<String> helloWorld() {
        return Mono.just(HELLO_WORLD_MESSAGE);
    }

    @PostMapping(value = "/hello", produces = "application/json")
    public Mono<String> helloWorldPost() {
        MessageWrapper messageWrapper = new MessageWrapper(HELLO_WORLD_MESSAGE);
        return Mono.just(new Gson().toJson(messageWrapper));
    }

}

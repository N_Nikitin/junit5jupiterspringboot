package com.epam.junit5.jupiter;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.EnabledIf;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.FluxExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

//@SpringJUnitConfig - composition of @ExtendWith(SpringExtension.class) and @ContextConfiguration
//@SpringJUnitWebConfig - same as @SpringJUnitConfig, but it's also composed with @WebAppConfiguration

//@BeforeEach -> @Before / @AfterEach -> @After
//@BeforeAll -> @BeforeClass / @AfterAll -> @AfterClass

@ExtendWith(SpringExtension.class) //provides full support for Spring TestContext Framework. Used to register custom extensions.
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class JupiterApplicationTests {

	@Autowired
	private WebTestClient webTestClient;

	private static final String HELLO_WORLD_MESSAGE = "Hello, World!";

	@BeforeAll
	@Test
    static void testBeforeAll() {
        System.out.println("Execute BEFORE ALL test. This will show once");
    }

    @BeforeEach
    @Test
    void testBeforeEach() {
        System.out.println("BEFORE EACH executed.");
    }

	@AfterEach
	@Test
	void testAfterEach() {
		System.out.println("AFTER EACH executed.");
	}

	@AfterAll
	@Test
	static void testAfterAll() {
		System.out.println("Execute AFTER ALL test. This will show once");
	}

	@Test
	@EnabledIf(value = "${enable.custom.tests}", loadContext = true)
	//@DisabledIf
	void testSum() {
		assertEquals(4, 2*2);
	}

	@Test
    @DisplayName("( ͡° ͜ʖ ͡°)")
    void testHelloWorldGet() {
		webTestClient.get()
				.uri("/hello")
				.accept(MediaType.TEXT_HTML)
				.exchange()
				.expectStatus().isOk()
				.expectBody()
				.consumeWith(response -> assertEquals(Arrays.hashCode(HELLO_WORLD_MESSAGE.getBytes()),
						Arrays.hashCode(response.getResponseBody())));
	}

	@Test
	void testHelloWorldPost() {
		webTestClient.post()
				.uri("/hello")
				.accept(MediaType.APPLICATION_JSON)
				.exchange()
				.expectStatus().isOk()
				.expectBody()
				.jsonPath("message", HELLO_WORLD_MESSAGE);
	}

	@Test
	void testEndlessData() {
		FluxExchangeResult<Integer> result = webTestClient.get()
				.uri("/endless")
				.accept(MediaType.TEXT_EVENT_STREAM)
				.exchange()
				.returnResult(Integer.class);
		int i = 0;
		Flux<Integer> integers = result.getResponseBody();

		StepVerifier.create(integers)
				.expectNext(++i, ++i, ++i)
				.thenCancel()
				.verify();
	}

	@TestFactory
    Stream<DynamicTest> testDynamicTests() {
        List<DynamicTest> tests = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            String testName = "dynamicTest" + i;
            tests.add(DynamicTest.dynamicTest(testName, () -> assertTrue(random.nextBoolean())));
        }
        return tests.stream();
    }

}
